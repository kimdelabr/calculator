/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epo.cicd.projet;

/**
 *
 * @author ARNAUD KIMA
 */
public class CalculatorFunctions {
    
     public int additionner(int a, int b){
        return a+b;
    }
     
    public int soustraire(int a, int b){
        return a-b;
    }
    
    public int multiplier(int a, int b){
        return a*b;
    }
    
    public int diviser(int a, int b){
        
        if(b != 0){
            return a/b;
        }
        else
            return -1;
        
    }
    
    
    
}
